#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh
echo "start-mining.sh"
echo $QUO_DATA
PRIVATE_CONFIG=ignore ${QUORUM} --datadir $QUO_DATA --permissioned --verbosity 5 --rpc --rpcaddr 0.0.0.0 --rpcport 8000 --port 9000 --raft --raftport 50400 --raftblocktime 70 --mine --rpcapi admin,db,eth,debug,miner,txpool,personal,web3,quorum,raft --unlock 0 --password <(echo -n "") > $QUO_DATA/../raft_quorum_log 2>&1 &
#echo --datadir $QUO_DATA --rpc --rpcaddr 0.0.0.0 --rpcport 8000 --port 9000 --raft --raftport 50400 --raftblocktime 2000 --unlock 0 --password <(echo -n "") 
